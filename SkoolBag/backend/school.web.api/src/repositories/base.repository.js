class BaseRepository {
  
  constructor(documentClient,baseParams) {
    this._documentClient = documentClient;
    this._baseParams= baseParams;
  }
  
  async getAll() {
    const params = this._createParamObject();
    const response = await this._documentClient.scan(params).promise();

    return response.Items || [];
  }
 
  async get(key) {
    const params = this._createParamObject(key);
    const response = await this._documentClient.get(params).promise();

    return response.Item;
  }
 
  async filter(queryObject) {
    const params = this._createParamObject(queryObject);
    const response = await this._documentClient.query(params).promise();

    return response.Item;
  }
  
  async put(school) {
    const params = this._createParamObject({ Item: school });
    await this._documentClient.put(params).promise();

    return school;
  }

 
  async delete(id) {
    const params = this._createParamObject({ Key: { id } });
    await this._documentClient.delete(params).promise();

    return id;
  }

  _createParamObject(additionalArgs = {}) {
    return Object.assign({}, this._baseParams, additionalArgs);
  }
}

exports.BaseRepository = BaseRepository;