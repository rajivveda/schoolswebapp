const {BaseRepository }= require('./base.repository');
const dynamodb = require('./dynamodb');
const {QueryBuilder } = require('./queries/query.builder');
/**
 * The School Repository
 */
class SchoolRepository extends BaseRepository{
  
  constructor() {
   const baseParams= {
      TableName: process.env.DYNAMODB_TABLE
    };
    super(dynamodb,baseParams);
  }
 
  async getSchoolsByFilters(request) {

    const state  = request.state.toLowerCase();
    const suburb = request.suburb.toLowerCase();

    // init query using the Query Builder
    const query = new QueryBuilder('gsi_1')
                     .setCondtion('gsi_1_pk = :gsi_1_pk and begins_with(gsi_1_sk, :gsi_1_sk)')
                     .setProjection("pk,dataItem")
                     .setAttributeValues({ ':gsi_1_pk': state ,
                                           ':gsi_1_sk': suburb});
                                        
    if(request.name != null && request.name.trim() !=="")
    {
        query.setFilter("contains(dataItem.schoolNameLC, :schoolName)")
             .addAttributeValue(":schoolName",request.name.toLowerCase())
    }
    if(request.street != null && request.street.trim() !=="")
    {
        const street =request.street.toLowerCase();
        query.addAttributeValue(':gsi_1_sk', suburb +'#'+ street); 
    }

    const params = this._createParamObject(query.build());
    const {Items} = await this._documentClient.query(params).promise();

    return Items.map(item=>(
              { _id:item.pk,
                schoolName :item.dataItem.schoolName,
                studentCount:item.dataItem.studentCount,
                type:item.dataItem.type,
                address:item.dataItem.address
              }));
  }
}


exports.SchoolRepository = SchoolRepository;