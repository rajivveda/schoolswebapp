'use strict';
class QueryBuilder {

    constructor(index) {
        this.index = index;
        this.queryObject={ IndexName:index};
    }

    setCondtion(value ) {
        this.queryObject["KeyConditionExpression"]=value;
        return this;
    }

    setFilter(value) {
        this.queryObject["FilterExpression"]=value;
        return this;
    }

    setProjection(value) {
        this.queryObject["ProjectionExpression"]=value;
        return this;
    }

    setAttributeValues(value) {
        this.queryObject["ExpressionAttributeValues"]=value;
        return this;
    }
    addAttributeValue(attr,value) {
        this.queryObject["ExpressionAttributeValues"][attr]=value;
        return this;
    }
    build() {
        return this.queryObject;
    }

}
exports.QueryBuilder = QueryBuilder;