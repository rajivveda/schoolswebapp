'use strict';
const { sendResponse} = require('../../util/response.util');
const { SchoolService } =require('../../services/schools/school.service');
const schoolService = new SchoolService();
const ok = sendResponse(200,JSON.stringify);
const serverError = sendResponse(500);
const badRequest = sendResponse(400);

module.exports.list = async (event) => {
  try {
    const { state, suburb, street, name } = event.queryStringParameters;
    if(!isValidRequest(state,suburb)){
      return badRequest();
    }
    var result = await schoolService.getSchoolsByFilters({state,suburb,street, name});
    return ok(result);
  } 
  catch {
    return serverError();
  }
};
function isValidRequest(state,suburb) {
  return (state != null && state.trim() !== "") && (suburb != null && suburb.trim() !== "");
}

