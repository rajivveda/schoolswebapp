'use strict';
const { sendResponse} = require('../../util/response.util');
const { SchoolService } = require('../../services/schools/school.service');

const schoolService = new SchoolService();
const ok = sendResponse(201,JSON.stringify);
const serverError = sendResponse(500);

module.exports.create = async (event) => {
  try {
    const { body } = event;
    const data = JSON.parse(body);
    var result = await schoolService.addSchool(data);
    return ok(result);
  } catch {
    return serverError();
  }
};
