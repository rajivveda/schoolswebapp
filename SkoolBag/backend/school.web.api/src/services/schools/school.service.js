'use strict';

const { SchoolRepository } = require('../../repositories/school.repository');
const { EntityFactory } = require('../../services/common/factories/entity.factory');

class SchoolService {
    constructor() 
    {
        this._schoolRepository= new SchoolRepository();
    }

    async getSchoolsByFilters (request)
    {
        return  await this._schoolRepository.getSchoolsByFilters(request);
    }
   
    async addSchool (request) 
    {
        const entityfactory = new EntityFactory('school', request);
        return  await this._schoolRepository.put(entityfactory.createEntity());
    }
}

exports.SchoolService = SchoolService;











