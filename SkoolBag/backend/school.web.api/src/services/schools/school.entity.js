const {BaseEntity} = require('../common/factories/base.entity')
const uuid = require('uuid');

class SchoolEntity extends BaseEntity {
  constructor(request) {
    super("skl", "#");
    this._request = request;
  }

  createEntity() 
  {
        super.createEntity();
        this.createPartionKey(new Array(uuid.v1()));
        this.createSortKey(new Array("info"));
        this.createSecondaryIndex("gsi_1_pk", new Array(this._request.address.state));
        this.createSecondaryIndex(
            "gsi_1_sk",
            new Array(this._request.address.suburb,
                    this._request.address.street)
        );
        this.createDataItem(this.getDataAttr());
        return this.entity;
  }

  getDataAttr() {
    return {
      schoolName: this._request.name,
      schoolNameLC: this._request.name.toLowerCase(),
      studentCount: this._request.studentCount,
      type: this._request.type,
      address: {
            state: this._request.address.state,
            postCode: this._request.address.postCode,
            suburb: this._request.address.suburb,
            street: this._request.address.street,
      },
    };
  }
}

exports.SchoolEntity = SchoolEntity;
