class BaseEntity {
    constructor(prefix,delimeter)  {
        this.prefix = prefix;
        this.delimeter= delimeter;
    }

    createPartionKey(values){
        this.entity.pk =this.prefix.concat(this.delimeter,values.join(this.delimeter).toLowerCase());
    }
    createSortKey(values){
        this.entity.sk =values.join(this.delimeter).toLowerCase();
    }
    createSecondaryIndex(index,values){
        this.entity[index]=values.join(this.delimeter).toLowerCase();
    }
    createDataItem(value){
        this.entity.dataItem = value;
    }
    createEntity(){
        this.entity={}
        const timestamp = new Date().getTime();
        this.entity.createdAt= timestamp;
        this.entity.updatedAt= timestamp;
    }
}
exports.BaseEntity = BaseEntity;

