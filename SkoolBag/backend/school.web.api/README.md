Test your service locally, without having to deploy it first.

## Setup


npm install
serverless dynamodb install
serverless offline start
serverless dynamodb migrate (this imports schema)


## Run service offline

```bash
serverless offline start
```

