import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { ApiService } from '../api.service';
import { Schools } from '../models/school';
import { ActivatedRoute } from '@angular/router';
/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}
@Component({
  selector: 'app-schools',
  templateUrl: './schools.component.html',
  styleUrls: ['./schools.component.scss']
})
export class SchoolsComponent implements OnInit {

  displayedColumns: string[] = ['schoolName', 'type', 'studentCount', 'address', 'postCode', 'state'];
  data: Schools[] = [];
  isLoadingResults = true;

  filterForm: FormGroup;
  stateList = ["NSW", "TAS", "NT", "SA", "WA", "QLD"];
  matcher = new MyErrorStateMatcher();


  constructor(private api: ApiService, private formBuilder: FormBuilder,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    const state = this.route.snapshot.params.state ??"NSW";
    const suburb = this.route.snapshot.params.suburb??"Abbotsbury";
    this.filterForm = this.formBuilder.group({
      name: [null],
      suburb: [suburb, Validators.required],
      street: [null],
      state: [state, Validators.required]
    });

    this.getSchoolsByFilter(this.filterForm.value);
  }
  getSchoolsByFilter(filter:any) {
    this.api.getSchools(filter)
      .subscribe((res: any) => {
        this.data = res;
        console.log(this.data)
        this.isLoadingResults = false;
      }, err => {
        console.log(err);
        this.isLoadingResults = false;
      });
  }
  onFormSubmit() {
    const formValues = this.filterForm.value;
    this.isLoadingResults = true;
    this.getSchoolsByFilter(this.filterForm.value);
  }
}
