import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SchoolsComponent } from './schools/schools.component';
import { AddSchoolsComponent } from './add-schools/add-schools.component';




const routes: Routes = [
  {
    path: 'schools',
    component: SchoolsComponent,
    data: { title: 'List of Schools' }
  },
  {
    path: 'add-schools',
    component: AddSchoolsComponent,
    data: { title: 'Add School' }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
