import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../api.service';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import {MatSnackBar} from '@angular/material/snack-bar';

import { CreateSchools} from '../models/create-school';
import {Address} from '../models/address';
/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-add-schools',
  templateUrl: './add-schools.component.html',
  styleUrls: ['./add-schools.component.scss']
})
export class AddSchoolsComponent implements OnInit {

  schoolsForm: FormGroup;
  typeList = ['Primary', 'Secondary', 'Combined'];
  stateList = ["NSW", "TAS", "NT","SA","WA","QLD"];
  isLoadingResults = false;
  matcher = new MyErrorStateMatcher();

  constructor(private router: Router, private api: ApiService, 
    private formBuilder: FormBuilder,private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.schoolsForm = this.formBuilder.group({
      name : [null, Validators.required],
      type : [null, Validators.required],
      studentCount : [null, Validators.required],
      postCode : [null, Validators.required],
      suburb : [null, Validators.required],
      street : [null, Validators.required],
      state : [null, Validators.required]
    });
  }

  onFormSubmit() {
    const formValues = this.schoolsForm.value;
    this.isLoadingResults = true;
    const school = new CreateSchools(formValues.name,
                                     formValues.studentCount,
                                     formValues.type,
                                     new Address(formValues.state,formValues.suburb,formValues.street,formValues.postCode));
    
    this.api.addSchools(school)
      .subscribe((res: any) => {
         //const id = res.id;
          this.isLoadingResults = false;
          this._snackBar.open("School successfully created","", {
            duration: 4000,
          });
          this.router.navigate(['/schools', {state: formValues.state, suburb: formValues.suburb}]);
        }, (err: any) => {
          console.log(err);
          this.isLoadingResults = false;
        });
  }

}