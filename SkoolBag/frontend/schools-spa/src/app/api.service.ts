import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Schools } from './models/School';
import { CreateSchools } from './models/create-school';
import { stringify } from 'querystring';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-type': 'application/json' })
};

const apiUrl = 'http://localhost:3000/dev/schools';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }

  getSchools(filter:any): Observable<Schools[]> {
    const url = `${apiUrl}${this.getUrlParamsforfilter(filter)}`;
    return this.http.get<Schools[]>(url)
      .pipe(
        tap(schools => console.log('fetched schools')),
        catchError(this.handleError('getSchools', []))
      );
  }
  private getUrlParamsforfilter(filter: any) {
    let url = `?state=${filter.state}&suburb=${filter.suburb}`;
    if (filter.street !== null && filter.street.trim() !== "") {
      url = `${url}&street=${filter.street}`;
    }
    if (filter.name !== null && filter.name.trim() !== "") {
      url = `${url}&name=${filter.name}`;
    }
    return url;
  }

  addSchools(createSchool: CreateSchools): Observable<Schools> {
    return this.http.post<Schools>(apiUrl, createSchool, httpOptions).pipe(
      tap((c: Schools) => console.log(`School successfully added`)),
      catchError(this.handleError<Schools>('add School'))
    );
  }
  
}
