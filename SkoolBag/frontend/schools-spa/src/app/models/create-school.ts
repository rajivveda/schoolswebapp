import {  Address } from './address';
export class CreateSchools {
    constructor(name: string,
        studentCount: number,
        type: string,
        address: Address) {
        this.name = name;
        this.address = address;
        this.type = type,
        this.studentCount = studentCount
    }
    studentCount: number;
    name: string;
    type: string;
    address: Address;
}
