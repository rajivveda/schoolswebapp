export class Address {
    constructor(state: string,
        suburb: string,
        street: string,
        postCode: string) {
        this.state = state;
        this.suburb = suburb;
        this.street = street,
        this.postCode = postCode;
    }
    suburb: string;
    postCode: string;
    state: string;
    street: string;
}