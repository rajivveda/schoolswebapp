import {  Address } from './address';
export interface Schools {
    _id: string;
    studentCount: number;
    schoolName: string;
    type: string;
    address: Address;
}